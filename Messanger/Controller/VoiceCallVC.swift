//
//  VoiceCallVC.swift
//  Messanger
//
//  Created by Farid on 9/10/18.
//  Copyright © 2018 Farid. All rights reserved.
//

import UIKit
import ContactsUI

class VoiceCallVC: UIViewController {
    
    var contact: CNContact!
    
    let pageTitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Call ..."
        label.font = UIFont(name: "IRANSansMobile-Light", size: 20)
        label.textAlignment = .center
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        
        return label
    }()
    
    let avatar: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFit
        img.layer.cornerRadius = 64
        img.clipsToBounds = true
        img.frame = CGRect(x: 0, y: 0, width: 128, height: 128)
        
        return img
    }()
    
    let fullname: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "IRANSansMobile-Light", size: 24)
        label.textAlignment = .center
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        
        return label
    }()
    
    let address: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "IRANSansMobile-Light", size: 24)
        label.textAlignment = .center
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        
        return label
    }()
    
    let phone: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "IRANSansMobile-Light", size: 24)
        label.textAlignment = .center
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        
        return label
    }()
    
    let separatorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1)
        return view
    }()
    
    let topButtonsStackView: UIStackView = {
        let stackview = UIStackView()
        stackview.translatesAutoresizingMaskIntoConstraints = false
        stackview.axis = .horizontal
        stackview.distribution = .fillEqually
        stackview.alignment = .top
        stackview.spacing = 0
        
        return stackview
    }()
    
    let muteButton: UIView = {
        let button = UIView()
        button.backgroundColor = .clear
        button.translatesAutoresizingMaskIntoConstraints = false
        
        let icon = UIImageView()
        icon.translatesAutoresizingMaskIntoConstraints = false
        icon.image = UIImage(named: "mute_call")
        
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Mute"
        label.textColor = UIColor.darkGray
        label.textAlignment = .center
        label.font = UIFont(name: "IRANSansMobile", size: 16)
        
        button.addSubview(icon)
        button.addSubview(label)
        
        icon.topAnchor.constraint(equalTo: button.topAnchor, constant: 8).isActive = true
        icon.widthAnchor.constraint(equalToConstant: 26).isActive = true
        icon.heightAnchor.constraint(equalToConstant: 42).isActive = true
        icon.centerXAnchor.constraint(equalTo: button.centerXAnchor).isActive = true
        
        label.centerXAnchor.constraint(equalTo: button.centerXAnchor).isActive = true
        label.bottomAnchor.constraint(equalTo: button.bottomAnchor, constant: -8).isActive = true
        
        return button
    }()
    
    let soundButton: UIView = {
        let button = UIView()
        button.backgroundColor = .clear
        button.translatesAutoresizingMaskIntoConstraints = false
        
        let icon = UIImageView()
        icon.translatesAutoresizingMaskIntoConstraints = false
        icon.image = UIImage(named: "sound")
        
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Sound"
        label.textColor = UIColor.darkGray
        label.textAlignment = .center
        label.font = UIFont(name: "IRANSansMobile", size: 16)
        
        button.addSubview(icon)
        button.addSubview(label)
        
        icon.topAnchor.constraint(equalTo: button.topAnchor, constant: 10).isActive = true
        icon.widthAnchor.constraint(equalToConstant: 39).isActive = true
        icon.heightAnchor.constraint(equalToConstant: 34).isActive = true
        icon.centerXAnchor.constraint(equalTo: button.centerXAnchor).isActive = true
        
        label.centerXAnchor.constraint(equalTo: button.centerXAnchor).isActive = true
        label.bottomAnchor.constraint(equalTo: button.bottomAnchor, constant: -8).isActive = true
        
        return button
    }()
    
    let holdButton: UIView = {
        let button = UIView()
        button.backgroundColor = .clear
        button.translatesAutoresizingMaskIntoConstraints = false
        
        let icon = UIImageView()
        icon.translatesAutoresizingMaskIntoConstraints = false
        icon.image = UIImage(named: "hold_call")
        
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Hold"
        label.textColor = UIColor.darkGray
        label.textAlignment = .center
        label.font = UIFont(name: "IRANSansMobile", size: 16)
        
        button.addSubview(icon)
        button.addSubview(label)
        
        icon.topAnchor.constraint(equalTo: button.topAnchor, constant: 4).isActive = true
        icon.widthAnchor.constraint(equalToConstant: 40).isActive = true
        icon.heightAnchor.constraint(equalToConstant: 40).isActive = true
        icon.centerXAnchor.constraint(equalTo: button.centerXAnchor).isActive = true
        
        label.centerXAnchor.constraint(equalTo: button.centerXAnchor).isActive = true
        label.bottomAnchor.constraint(equalTo: button.bottomAnchor, constant: -8).isActive = true
        
        return button
    }()
    
    let callButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "call"), for: .normal)
        
        return button
    }()
    
    func setupViews() {
        view.backgroundColor = UIColor.SpringWood
        
        view.addSubview(pageTitle)
        view.addSubview(avatar)
        view.addSubview(fullname)
        view.addSubview(address)
        view.addSubview(phone)
        view.addSubview(separatorView)
        view.addSubview(topButtonsStackView)
        topButtonsStackView.addArrangedSubview(muteButton)
        topButtonsStackView.addArrangedSubview(soundButton)
        topButtonsStackView.addArrangedSubview(holdButton)
        view.addSubview(callButton)
        
        pageTitle.topAnchor.constraint(equalTo: view.topAnchor, constant: 100).isActive = true
        pageTitle.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        avatar.frame = CGRect(x: (view.frame.width / 2) - 64, y: 100 + 48, width: 128, height: 128)
        
        fullname.topAnchor.constraint(equalTo: pageTitle.bottomAnchor, constant: 44 + 16 + 128).isActive = true
        fullname.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        address.topAnchor.constraint(equalTo: fullname.bottomAnchor, constant: 8).isActive = true
        address.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        phone.topAnchor.constraint(equalTo: address.bottomAnchor, constant: 8).isActive = true
        phone.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        separatorView.bottomAnchor.constraint(equalTo: topButtonsStackView.topAnchor, constant: -32).isActive = true
        separatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        separatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        separatorView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16).isActive = true
        separatorView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16).isActive = true
        
        topButtonsStackView.bottomAnchor.constraint(equalTo: callButton.topAnchor, constant: -24).isActive = true
        topButtonsStackView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16).isActive = true
        topButtonsStackView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16).isActive = true
        topButtonsStackView.heightAnchor.constraint(equalToConstant: 84).isActive = true
        
        muteButton.heightAnchor.constraint(equalToConstant: 84).isActive = true
        soundButton.heightAnchor.constraint(equalToConstant: 84).isActive = true
        holdButton.heightAnchor.constraint(equalToConstant: 84).isActive = true
        
        callButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -view.frame.height / 16).isActive = true
        callButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        callButton.widthAnchor.constraint(equalToConstant: 80).isActive = true
        callButton.heightAnchor.constraint(equalToConstant: 80).isActive = true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let name = contact.givenName + contact.familyName

        self.fullname.text = name
        
        if contact.phoneNumbers.count > 0 {
            self.phone.text = (contact.phoneNumbers[0].value).value(forKey: "digits") as? String
        }
        
        if contact.postalAddresses.count > 0 {
            let city = (contact.postalAddresses[0].value).value(forKey: "city") as? String
            let street = (contact.postalAddresses[0].value).value(forKey: "street") as? String
            
            self.address.text =  city! + ", " + street!
        }

        if let imageData = contact.imageData {
            avatar.image = UIImage(data: imageData)
        } else {
            avatar.setImage(string:name, color: UIColor.colorHash(name: name), circular: true)
        }
        
        setupViews()
    }

}
