//
//  UIColor+extension.swift
//  Messanger
//
//  Created by Farid on 9/10/18.
//  Copyright © 2018 Farid. All rights reserved.
//

import UIKit

extension UIColor {
    static let SpringWood = UIColor(red:0.98, green:0.97, blue:0.96, alpha:1.00)
    static let DodgerBlue = UIColor(red: 0.16, green: 0.47, blue: 1, alpha: 1.0)
    static let AthensGray = UIColor(red: 0.94, green: 0.95, blue: 0.96, alpha: 1.0)
    static let Apricot = UIColor(red: 0.95, green: 0.56, blue: 0.50, alpha: 1.0)
}
